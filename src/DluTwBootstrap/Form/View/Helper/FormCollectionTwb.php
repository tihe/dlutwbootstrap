<?php
namespace DluTwBootstrap\Form\View\Helper;

use DluTwBootstrap\Form\Exception\UnsupportedElementTypeException;
use DluTwBootstrap\GenUtil;
use DluTwBootstrap\Form\FormUtil;

use RuntimeException;
use Zend\Form\Element;
use Zend\Form\Element\Collection as CollectionElement;
use Zend\Form\ElementInterface;
use Zend\Form\FieldsetInterface;
use Zend\Form\View\Helper\FormCollection;
use Zend\I18n\Translator\TranslatorAwareInterface;
use Zend\I18n\Translator\Translator;

/**
 * FormCollectionTwb
 * @package DluTwBootstrap
 * @copyright David Lukas (c) - http://www.zfdaily.com
 * @license http://www.zfdaily.com/code/license New BSD License
 * @link http://www.zfdaily.com
 * @link https://bitbucket.org/dlu/dlutwbootstrap
 */
class FormCollectionTwb extends FormCollection
{
    /**
     * The name of the default view helper that is used to render sub elements.
     *
     * @var string
     */
    protected $defaultElementHelper = 'formrowtwb';    
    
    /**
     * @var GenUtil
     */
    protected $genUtil;
    
    /**
     * @var FormUtil
     */
    protected $formUtil;

    /* **************************** METHODS ****************************** */

    /**
     * Constructor
     * @param \DluTwBootstrap\GenUtil $genUtil
     * @param \DluTwBootstrap\Form\FormUtil $formUtil
     */
    public function __construct(GenUtil $genUtil, FormUtil $formUtil)
    {
        $this->genUtil = $genUtil;
        $this->formUtil = $formUtil;
    }

    /**
     * @param FieldsetInterface|null $fieldset
     * @param string|null $formType
     * @param array $displayOptions
     * @param bool $displayButtons Should buttons found in this fieldset be rendered?
     * @param bool $renderFieldsetTag Should we render the <fieldset> tag around the fieldset?
     * @param bool $renderErrors
     * @return string
     */
    public function __invoke(ElementInterface $element = null,
                             $formType = null,
                             array $displayOptions = array(),
                             $renderErrors = true,
                             $wrap = true
    ) {
        if(is_null($element)) {
            return $this;
        }
        
        $this->setShouldWrap($wrap);
        
        return $this->render($element, $formType, $displayOptions, $renderErrors);
    }

    /**
     * @param FieldsetInterface $fieldset
     * @param string|null $formType
     * @param array $displayOptions
     * @param bool $displayButtons Should buttons found in this fieldset be rendered?
     * @param bool $renderFieldsetTag Should we render the <fieldset> tag around the fieldset?
     * @param bool $renderErrors
     * @return string
     */
    public function render(ElementInterface $element,
                           $formType = null,
                           array $displayOptions = array(),
                           $renderErrors = true
    ) {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }
        
        $markup           = '';
        $templateMarkup   = '';
        $escapeHtmlHelper = $this->getEscapeHtmlHelper();
        $elementHelper    = $this->getElementHelper();
        $fieldsetHelper   = $this->getFieldsetHelper();        
        $formType         = $this->formUtil->filterFormType($formType);
        
        if ($element instanceof CollectionElement && $element->shouldCreateTemplate()) {            
            $templateMarkup = $this->renderTemplate($element, $formType, array(), $renderErrors); //TODO display options
        }        
        
        if (array_key_exists('fieldsets', $displayOptions)) {
            $displayOptionsFieldsets    = $displayOptions['fieldsets'];
        } else {
            $displayOptionsFieldsets    = array();
        }
        if (array_key_exists('elements', $displayOptions)) {
            $displayOptionsElements     = $displayOptions['elements'];
        } else {
            $displayOptionsElements     = array();
        }
        
        //Iterate over all fieldset elements and render them
        foreach($element->getIterator() as $elementOrFieldset) {
            $elementName        = $elementOrFieldset->getName();
            $elementBareName    = $this->formUtil->getBareElementName($elementName);
            if ($elementOrFieldset instanceof FieldsetInterface) {
                // get fieldset display options
                if (array_key_exists($elementBareName, $displayOptionsFieldsets)) {
                    $displayOptionsFieldset = $displayOptionsFieldsets[$elementBareName];
                } else {
                    $displayOptionsFieldset = array();
                }
                $markup .= "\n" . $fieldsetHelper($elementOrFieldset,
                                                  $formType,
                                                  $displayOptionsFieldset,
                                                  $renderErrors);
            } elseif ($elementOrFieldset instanceof ElementInterface) {
                //TODO move 'ignore' buttons to FormUtils: why should we ignore them? We just want them rendered differently
                /* if (in_array($elementOrFieldset->getAttribute('type'), array('submit', 'reset', 'button'))) {
                    // Ignore 'button' elements: skip the rest of the iteration
                    continue;
                } */
                // get element display options
                if (array_key_exists($elementBareName, $displayOptionsElements)) {
                    $displayOptionsElement  = $displayOptionsElements[$elementBareName];
                } else {
                    $displayOptionsElement  = array();
                }
  
                $markup .= "\n" . $elementHelper($elementOrFieldset, $formType, $displayOptionsElement, $renderErrors);
            } else {
                //Unsupported item type
                throw new UnsupportedElementTypeException('Fieldsets may contain only fieldsets or elements.');
            }
        }
        
        // If $templateMarkup is not empty, use it for simplify adding new element in JavaScript
        if (!empty($templateMarkup)) {
            $markup .= $templateMarkup;
        }
        
        // Every collection is wrapped by a fieldset if needed
        if ($this->shouldWrap) {
            $label = $element->getLabel();
        
            if (!empty($label)) {
        
                if (null !== ($translator = $this->getTranslator())) {
                    $label = $translator->translate(
                        $label, $this->getTranslatorTextDomain()
                    );
                }
        
                $label = $escapeHtmlHelper($label);
        
                $markup = sprintf(
                    $this->openTag($element, $formType, $displayOptions) . '<legend>%s</legend>%s' . $this->closeTag(),
                    $label,
                    $markup
                );
            }
        }
        
        return $markup;
    }
    
    /**
     * Only render a template
     *
     * @param  CollectionElement $collection
     * @return string
     */
    public function renderTemplate(CollectionElement $collection, $formType, $displayOptions, $renderErrors)
    {
        $elementHelper          = $this->getElementHelper();
        $escapeHtmlAttribHelper = $this->getEscapeHtmlAttrHelper();
        $templateMarkup         = '';

        $elementOrFieldset = $collection->getTemplateElement();

        if ($elementOrFieldset instanceof FieldsetInterface) {
            $templateMarkup .= $this->render($elementOrFieldset, $formType, $displayOptions, $renderErrors);
        } elseif ($elementOrFieldset instanceof ElementInterface) {
            $templateMarkup .= $elementHelper($elementOrFieldset, $formType, $displayOptions, $renderErrors);
        }

        return sprintf(
            '<span data-template="%s"></span>',
            $escapeHtmlAttribHelper($templateMarkup)
        );
    }

    /**
     * Returns the fieldset opening tag and legend tag, if legend is defined
     * @param FieldsetInterface $fieldset
     * @param string|null $formType
     * @param array $displayOptions
     * @return string
     */
    public function openTag(FieldsetInterface $fieldset, $formType = null, array $displayOptions = array()) {
        $formType   = $this->formUtil->filterFormType($formType);
        $class      = $fieldset->getAttribute('class');
        if (array_key_exists('class', $displayOptions)) {
            $class  = $this->genUtil->addWords($displayOptions['class'], $class);
        }
        $escapeHtmlAttrHelper   = $this->getEscapeHtmlAttrHelper();
        $class                  = $this->genUtil->escapeWords($class, $escapeHtmlAttrHelper);
        $fieldset->setAttribute('class', $class);
        if ($class) {
            $classAttrib        = sprintf(' class="%s"', $class);
        } else {
            $classAttrib        = '';
        }
        $markup = sprintf('<fieldset%s>', $classAttrib);
        $legend = $fieldset->getOption('legend');
        if ($legend
            && (!array_key_exists('display_legend', $displayOptions) || $displayOptions['display_legend'])
            && ($formType == FormUtil::FORM_TYPE_HORIZONTAL || $formType == FormUtil::FORM_TYPE_VERTICAL)) {
            //Translate
            if (null !== ($translator = $this->getTranslator())) {
                $legend = $translator->translate($legend, $this->getTranslatorTextDomain());
            }
            //Escape
            $escapeHelper   = $this->getEscapeHtmlHelper();
            $legend         = $escapeHelper($legend);
            $markup        .= "<legend>$legend</legend>";
        }
        return $markup;
    }

    /**
     * Returns the fieldset closing tag
     * @return string
     */
    public function closeTag() {
        return '</fieldset>';
    }
}